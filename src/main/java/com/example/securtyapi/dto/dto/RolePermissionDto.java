package com.example.securtyapi.dto.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RolePermissionDto {

    private Long id;
    private String roleName;
    private List<String> permissionName;
}
