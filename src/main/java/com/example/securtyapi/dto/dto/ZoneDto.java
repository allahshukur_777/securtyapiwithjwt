package com.example.securtyapi.dto.dto;

import lombok.Data;

@Data
public class ZoneDto {

    private Long id;
    private String name;
}
