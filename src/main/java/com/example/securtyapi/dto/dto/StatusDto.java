package com.example.securtyapi.dto.dto;

import lombok.Data;

@Data
public class StatusDto {

    private Long id;
    private String name;
}
