package com.example.securtyapi.dto.dto;

import lombok.Data;

@Data
public class LoginRequest {

    private String username;
    private String password;
}
