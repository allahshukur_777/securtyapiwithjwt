package com.example.securtyapi.dto.saveDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSaveDto {

    private String fullName;
    private String username;
    private String password;
    private int role;
}
