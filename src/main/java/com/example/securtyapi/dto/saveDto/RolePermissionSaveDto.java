package com.example.securtyapi.dto.saveDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RolePermissionSaveDto {

    private Long roleId;
    private Long permissionId;
}
