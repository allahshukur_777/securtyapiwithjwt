package com.example.securtyapi.dto.updateDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdateDto {

    private Long id;
    private String fullName;
    private String username;
    private String password;
    private int role;
}
