package com.example.securtyapi.dto.updateDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RolePermissionUpdateDto {

    private Long id;
    private Long roleId;
    private Long permissionId;
}
