package com.example.securtyapi.service;

import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

    List<UserDto> getAll(Pageable pageable);

    UserDto get(Long id);

    UserDto getByUsername(String username);

    UserSaveDto save(UserSaveDto userSaveDto);

    UserUpdateDto update(Long id, UserUpdateDto userUpdateDto);

    void deleteById(Long id);

}
