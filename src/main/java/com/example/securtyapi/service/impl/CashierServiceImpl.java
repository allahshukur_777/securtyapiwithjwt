package com.example.securtyapi.service.impl;

import com.example.securtyapi.dto.dto.CashierDto;
import com.example.securtyapi.dto.saveDto.CashierSaveDto;
import com.example.securtyapi.dto.updateDto.CashierUpdateDto;
import com.example.securtyapi.repository.CashierRepository;
import com.example.securtyapi.service.CashierService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CashierServiceImpl implements CashierService {

    private final CashierRepository cashierRepository;

    @Override
    public List<CashierDto> getAll(Pageable pageable) {
        return cashierRepository.findAll(pageable);
    }

    @Override
    public CashierDto get(Long id) {
        return cashierRepository.findById(id);
    }

    @Override
    public CashierSaveDto save(CashierSaveDto cashierSave){
        return cashierRepository.save(cashierSave);
    }

    @Override
    public CashierUpdateDto update(Long id, CashierUpdateDto cashierUpdate) {
        return cashierRepository.update(id, cashierUpdate);
    }

    @Override
    public void deleteById(Long id) {
         cashierRepository.deleteById(id);
    }
}
