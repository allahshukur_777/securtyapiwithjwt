package com.example.securtyapi.service.impl;

import com.example.securtyapi.dto.dto.RolePermissionDto;
import com.example.securtyapi.dto.saveDto.RolePermissionSaveDto;
import com.example.securtyapi.dto.updateDto.RolePermissionUpdateDto;
import com.example.securtyapi.repository.RolePermissionRepository;
import com.example.securtyapi.service.RolePermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RolePermissionServiceImpl implements RolePermissionService {

    private final RolePermissionRepository rolePermissionRepository;

    @Override
    public List<RolePermissionDto> getAll(Pageable pageable) {
        return rolePermissionRepository.findAll(pageable);
    }

    @Override
    public RolePermissionDto get(Long id) {
        return rolePermissionRepository.findById(id);
    }

    @Override
    public RolePermissionDto getByRoleName(String roleName) {
        return rolePermissionRepository.findRoleAndPermissionByRoleName(roleName);
    }

    @Override
    public RolePermissionSaveDto save(RolePermissionSaveDto rolePermissionSave){
        return rolePermissionRepository.save(rolePermissionSave);
    }

    @Override
    public RolePermissionUpdateDto update(Long id, RolePermissionUpdateDto rolePermissionUpdate) {
        return rolePermissionRepository.update(id, rolePermissionUpdate);
    }

    @Override
    public void deleteById(Long id) {
         rolePermissionRepository.deleteById(id);
    }
}
