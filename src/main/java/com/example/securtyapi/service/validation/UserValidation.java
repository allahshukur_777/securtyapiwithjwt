package com.example.securtyapi.service.validation;

import com.example.securtyapi.dto.dto.UserDto;

public interface UserValidation {

    UserDto validUserDto(UserDto userDto, String password);
}
