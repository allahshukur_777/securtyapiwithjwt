package com.example.securtyapi.service.validation.impl;

import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.service.validation.UserValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserValidationImpl implements UserValidation {

    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDto validUserDto(UserDto userDto, String password) {
        if (userDto != null && passwordEncoder.matches(password, userDto.getPassword()))
            return userDto;
        throw new UsernameNotFoundException(userDto.getUsername());
    }
}
