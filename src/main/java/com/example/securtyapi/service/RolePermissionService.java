package com.example.securtyapi.service;

import com.example.securtyapi.dto.dto.RolePermissionDto;
import com.example.securtyapi.dto.saveDto.RolePermissionSaveDto;
import com.example.securtyapi.dto.updateDto.RolePermissionUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RolePermissionService {

    List<RolePermissionDto> getAll(Pageable pageable);

    RolePermissionDto get(Long id);

    RolePermissionDto getByRoleName(String roleName);

    RolePermissionSaveDto save(RolePermissionSaveDto rolePermissionSaveDto);

    RolePermissionUpdateDto update(Long id, RolePermissionUpdateDto rolePermissionUpdateDto);

    void deleteById(Long id);
}
