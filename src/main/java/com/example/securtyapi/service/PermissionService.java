package com.example.securtyapi.service;

import com.example.securtyapi.dto.dto.PermissionDto;
import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.saveDto.PermissionSaveDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.updateDto.PermissionUpdateDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PermissionService {

    List<PermissionDto> getAll(Pageable pageable);

    PermissionSaveDto save(PermissionSaveDto permissionSaveDto);

    PermissionUpdateDto update(Long id, PermissionUpdateDto permissionUpdateDto);

    void deleteById(Long id);
}
