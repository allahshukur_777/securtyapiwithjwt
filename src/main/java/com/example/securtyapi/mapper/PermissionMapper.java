package com.example.securtyapi.mapper;

import com.example.securtyapi.dto.dto.PermissionDto;
import com.example.securtyapi.dto.saveDto.PermissionSaveDto;
import com.example.securtyapi.dto.updateDto.PermissionUpdateDto;
import jooq.tables.pojos.Permission;
import jooq.tables.records.PermissionRecord;

public interface PermissionMapper {

    PermissionDto toPermissionDto(Permission permission);

    PermissionSaveDto toPermissionSaveDto(PermissionRecord permissionRecord);

    PermissionUpdateDto toPermissionUpdateDto(PermissionRecord permissionRecord);
}
