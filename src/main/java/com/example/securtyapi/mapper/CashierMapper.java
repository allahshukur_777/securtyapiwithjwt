package com.example.securtyapi.mapper;

import com.example.securtyapi.dto.dto.CashierDto;
import com.example.securtyapi.dto.saveDto.CashierSaveDto;
import com.example.securtyapi.dto.updateDto.CashierUpdateDto;
import jooq.tables.pojos.Cashier;
import jooq.tables.records.CashierRecord;

public interface CashierMapper {

    CashierDto toCashierDto(Cashier cashier);

    CashierSaveDto toCashierSaveDto(CashierRecord cashierRecord);

    CashierUpdateDto toCashierUpdateDto(CashierRecord cashierRecord);
}
