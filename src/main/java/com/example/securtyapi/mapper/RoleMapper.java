package com.example.securtyapi.mapper;

import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import jooq.tables.pojos.Role;
import jooq.tables.records.RoleRecord;

public interface RoleMapper {

    RoleDto toRoleDto(Role role);

    RoleSaveDto toRoleSaveDto(RoleRecord roleRecord);

    RoleUpdateDto toRoleUpdateDto(RoleRecord roleRecord);
}
