package com.example.securtyapi.mapper.impl;

import com.example.securtyapi.dto.dto.CashierDto;
import com.example.securtyapi.dto.saveDto.CashierSaveDto;
import com.example.securtyapi.dto.updateDto.CashierUpdateDto;
import com.example.securtyapi.enums.RoleEnum;
import com.example.securtyapi.enums.StatusEnum;
import com.example.securtyapi.mapper.CashierMapper;
import com.example.securtyapi.util.DateUtil;
import jooq.tables.pojos.Cashier;
import jooq.tables.records.CashierRecord;
import org.springframework.stereotype.Component;

@Component
public class CashierMapperImpl implements CashierMapper {

    @Override
    public CashierDto toCashierDto(Cashier cashier) {
        CashierDto cashierDto = new CashierDto();
        cashierDto.setId(cashier.getId());
        cashierDto.setCashierCode(cashier.getCashierCode());
        cashierDto.setProviderId(cashier.getProviderId());
        cashierDto.setFullName(cashier.getFullName());
        cashierDto.setMobile(cashier.getMobile());
        cashierDto.setPhone(cashier.getPhone());
        cashierDto.setEmail(cashier.getEmail());
        cashierDto.setSalesRepresentative(cashier.getSalesRepresentative());
        cashierDto.setZone(cashier.getZone());
        cashierDto.setCities(cashier.getCities());
        cashierDto.setRegion(cashier.getRegion());
        cashierDto.setAddress(cashier.getAddress());
        cashierDto.setMacAddress(cashier.getMacAddress());
        cashierDto.setNextPermanentBalance(cashier.getNextPermanentBalance());
        cashierDto.setCurrentBalance(cashier.getCurrentBalance());
        cashierDto.setDebtCredit(cashier.getDebtCredit());
        cashierDto.setExtraDebtCredit(cashier.getExtraDebtCredit());
        cashierDto.setMinStake(cashier.getMinStake());
        cashierDto.setMaxStake(cashier.getMaxStake());
        cashierDto.setBetTicketPayoutLimit(cashier.getBetTicketPayoutLimit());
        cashierDto.setVoucherPayoutLimit(cashier.getVoucherPayoutLimit());
        cashierDto.setStatus(StatusEnum.getEnum(cashier.getStatus()).name());
        cashierDto.setFreezeStatus(cashier.getFreezeStatus());
        cashierDto.setUserName(cashier.getUserName());
        cashierDto.setPassword(cashier.getPassword());
        cashierDto.setLastLogin(DateUtil.getLong(cashier.getLastLogin()));
        cashierDto.setLastLogout(DateUtil.getLong(cashier.getLastLogout()));
        cashierDto.setCreationDate(DateUtil.getLong(cashier.getCreationDate()));
        cashierDto.setRole(RoleEnum.getEnum(cashier.getRole()).getId());
        return cashierDto;
    }

    @Override
    public CashierSaveDto toCashierSaveDto(CashierRecord cashierRecord) {
        return new CashierSaveDto(
                cashierRecord.getCashierCode(),
                cashierRecord.getProviderId(),
                cashierRecord.getFullName(),
                cashierRecord.getMobile(),
                cashierRecord.getPhone(),
                cashierRecord.getEmail(),
                cashierRecord.getSalesRepresentative(),
                cashierRecord.getZone(),
                cashierRecord.getCities(),
                cashierRecord.getRegion(),
                cashierRecord.getAddress(),
                cashierRecord.getMacAddress(),
                cashierRecord.getNextPermanentBalance(),
                cashierRecord.getCurrentBalance(),
                cashierRecord.getDebtCredit(),
                cashierRecord.getExtraDebtCredit(),
                cashierRecord.getMinStake(),
                cashierRecord.getMaxStake(),
                cashierRecord.getBetTicketPayoutLimit(),
                cashierRecord.getVoucherPayoutLimit(),
                cashierRecord.getStatus(),
                cashierRecord.getFreezeStatus(),
                cashierRecord.getUserName(),
                cashierRecord.getPassword(),
                DateUtil.getLong(cashierRecord.getLastLogin()),
                DateUtil.getLong(cashierRecord.getLastLogout()),
                DateUtil.getLong(cashierRecord.getCreationDate()),
                RoleEnum.getEnum(cashierRecord.getRole()).getId());
    }

    @Override
    public CashierUpdateDto toCashierUpdateDto(CashierRecord cashierRecord) {
        return new CashierUpdateDto(
                cashierRecord.getId(),
                cashierRecord.getCashierCode(),
                cashierRecord.getProviderId(),
                cashierRecord.getFullName(),
                cashierRecord.getMobile(),
                cashierRecord.getPhone(),
                cashierRecord.getEmail(),
                cashierRecord.getSalesRepresentative(),
                cashierRecord.getZone(),
                cashierRecord.getCities(),
                cashierRecord.getRegion(),
                cashierRecord.getAddress(),
                cashierRecord.getMacAddress(),
                cashierRecord.getNextPermanentBalance(),
                cashierRecord.getCurrentBalance(),
                cashierRecord.getDebtCredit(),
                cashierRecord.getExtraDebtCredit(),
                cashierRecord.getMinStake(),
                cashierRecord.getMaxStake(),
                cashierRecord.getBetTicketPayoutLimit(),
                cashierRecord.getVoucherPayoutLimit(),
                cashierRecord.getStatus(),
                cashierRecord.getFreezeStatus(),
                cashierRecord.getUserName(),
                cashierRecord.getPassword(),
                DateUtil.getLong(cashierRecord.getLastLogin()),
                DateUtil.getLong(cashierRecord.getLastLogout()),
                DateUtil.getLong(cashierRecord.getCreationDate()),
                RoleEnum.getEnum(cashierRecord.getRole()).getId());
    }
}