package com.example.securtyapi.mapper.impl;

import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import com.example.securtyapi.mapper.RoleMapper;
import jooq.tables.pojos.Role;
import jooq.tables.records.RoleRecord;
import org.springframework.stereotype.Component;

@Component
public class RoleMapperImpl implements RoleMapper {

    @Override
    public RoleDto toRoleDto(Role role) {
        RoleDto roleDto = new RoleDto();
        roleDto.setId(role.getId());
        roleDto.setName(role.getName());
        return roleDto;
    }

    @Override
    public RoleSaveDto toRoleSaveDto(RoleRecord roleRecord) {
        return new RoleSaveDto(
                roleRecord.getName());
    }

    @Override
    public RoleUpdateDto toRoleUpdateDto(RoleRecord roleRecord) {
        return new RoleUpdateDto(
                roleRecord.getId(),
                roleRecord.getName());
    }
}