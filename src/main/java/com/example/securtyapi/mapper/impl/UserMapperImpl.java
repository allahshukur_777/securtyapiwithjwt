package com.example.securtyapi.mapper.impl;

import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import com.example.securtyapi.enums.RoleEnum;
import com.example.securtyapi.mapper.UserMapper;
import jooq.tables.pojos.User;
import jooq.tables.records.UserRecord;
import org.springframework.stereotype.Component;

@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public UserDto toUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFullName(user.getFullName());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setFreezeStatus(user.getFreezeStatusId());
        userDto.setRole(RoleEnum.getEnum(user.getRole()).name());
        return userDto;
    }

    @Override
    public UserSaveDto toUserSaveDto(UserRecord userRecord) {
        return new UserSaveDto(
                userRecord.getFullName(),
                userRecord.getUsername(),
                userRecord.getPassword(),
                RoleEnum.getEnum(userRecord.getRole()).getId());
    }

    @Override
    public UserUpdateDto toUserUpdateDto(UserRecord userRecord) {
        return new UserUpdateDto(
                userRecord.getId(),
                userRecord.getFullName(),
                userRecord.getUsername(),
                userRecord.getPassword(),
                RoleEnum.getEnum(userRecord.getRole()).getId());
    }
}