package com.example.securtyapi.mapper.impl;

import com.example.securtyapi.dto.dto.PermissionDto;
import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.dto.RolePermissionDto;
import com.example.securtyapi.dto.saveDto.RolePermissionSaveDto;
import com.example.securtyapi.dto.updateDto.RolePermissionUpdateDto;
import com.example.securtyapi.mapper.RolePermissionMapper;
import jooq.tables.records.RolePermissionRecord;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static jooq.tables.Permission.PERMISSION;
import static jooq.tables.Role.ROLE;

@Component
public class RolePermissionMapperImpl implements RolePermissionMapper {

    @Override
    public List<RolePermissionDto> toRolePermission(Result<Record> recordResult) {
        Map<Long, RolePermissionDto> rolePermissionMap = new HashMap<>();
        Map<Long, RoleDto> roleMap = new HashMap<>();

        recordResult.forEach(r -> {
            RolePermissionDto rolePermission = rolePermissionMap.get(r.getValue(ROLE.ID));
            if (rolePermission == null) {
                rolePermission = new RolePermissionDto();
                rolePermission.setId(r.getValue(ROLE.ID));
                rolePermissionMap.put(rolePermission.getId(), rolePermission);
            }

            RoleDto role = roleMap.get(r.getValue(ROLE.ID));
            if (role == null && r.getValue(ROLE.ID) != null) {
                role = new RoleDto();
                role.setId(r.getValue(ROLE.ID));
                role.setName(r.getValue(ROLE.NAME));
                roleMap.put(role.getId(), role);
                rolePermission.setRoleName(role.getName());
            }

            if (r.getValue(PERMISSION.ID) != null) {
                PermissionDto permission = new PermissionDto();
                permission.setId(r.getValue(PERMISSION.ID));
                permission.setName(r.getValue(PERMISSION.NAME));
                assert role != null;
                addPermissionList(rolePermission.getPermissionName(), rolePermission, permission.getName());
            }
        });
        return new ArrayList<>(rolePermissionMap.values());
    }

    @Override
    public RolePermissionSaveDto toRolePermissionSave(RolePermissionRecord rolePermissionRecord) {
        return new RolePermissionSaveDto(
                rolePermissionRecord.getRoleId(),
                rolePermissionRecord.getPermissionId());
    }

    @Override
    public RolePermissionUpdateDto toRolePermissionUpdate(RolePermissionRecord rolePermissionRecord) {
        return new RolePermissionUpdateDto(
                rolePermissionRecord.getId(),
                rolePermissionRecord.getRoleId(),
                rolePermissionRecord.getPermissionId());
    }

    private void addPermissionList(List<String> permissionList, RolePermissionDto rolePermissionDto, String permissionName) {
        if (permissionList == null) {
            permissionList = new ArrayList<>();
        }
        permissionList.add(permissionName);
        rolePermissionDto.setPermissionName(permissionList);
    }
}