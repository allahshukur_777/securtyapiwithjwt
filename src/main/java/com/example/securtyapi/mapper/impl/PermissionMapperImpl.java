package com.example.securtyapi.mapper.impl;

import com.example.securtyapi.dto.dto.PermissionDto;
import com.example.securtyapi.dto.saveDto.PermissionSaveDto;
import com.example.securtyapi.dto.updateDto.PermissionUpdateDto;
import com.example.securtyapi.mapper.PermissionMapper;
import jooq.tables.pojos.Permission;
import jooq.tables.records.PermissionRecord;
import org.springframework.stereotype.Component;

@Component
public class PermissionMapperImpl implements PermissionMapper {

    @Override
    public PermissionDto toPermissionDto(Permission permission) {
        PermissionDto permissionDto = new PermissionDto();
        permissionDto.setId(permission.getId());
        permissionDto.setName(permission.getName());
        return permissionDto;
    }

    @Override
    public PermissionSaveDto toPermissionSaveDto(PermissionRecord permissionRecord) {
        return new PermissionSaveDto(
                permissionRecord.getName());
    }

    @Override
    public PermissionUpdateDto toPermissionUpdateDto(PermissionRecord permissionRecord) {
        return new PermissionUpdateDto(
                permissionRecord.getId(),
                permissionRecord.getName());
    }
}