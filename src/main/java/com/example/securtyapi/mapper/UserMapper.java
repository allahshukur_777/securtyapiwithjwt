package com.example.securtyapi.mapper;

import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import jooq.tables.pojos.User;
import jooq.tables.records.UserRecord;

public interface UserMapper {

    UserDto toUserDto(User user);

    UserSaveDto toUserSaveDto(UserRecord userRecord);

    UserUpdateDto toUserUpdateDto(UserRecord userRecord);
}
