package com.example.securtyapi.mapper;

import com.example.securtyapi.dto.dto.RolePermissionDto;
import com.example.securtyapi.dto.saveDto.RolePermissionSaveDto;
import com.example.securtyapi.dto.updateDto.RolePermissionUpdateDto;
import jooq.tables.records.RolePermissionRecord;
import org.jooq.Record;
import org.jooq.Result;

import java.util.List;

public interface RolePermissionMapper {

    List<RolePermissionDto> toRolePermission(Result<Record> agentRecord);

    RolePermissionSaveDto toRolePermissionSave(RolePermissionRecord rolePermissionRecord);

    RolePermissionUpdateDto toRolePermissionUpdate(RolePermissionRecord rolePermissionRecord);
}
