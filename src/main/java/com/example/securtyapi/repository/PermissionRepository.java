package com.example.securtyapi.repository;

import com.example.securtyapi.dto.dto.PermissionDto;
import com.example.securtyapi.dto.saveDto.PermissionSaveDto;
import com.example.securtyapi.dto.updateDto.PermissionUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PermissionRepository {

    List<PermissionDto> findAll(Pageable pageable);

    PermissionDto findPermissionByPermissionName(String permissionName);

    PermissionSaveDto save(PermissionSaveDto permissionSaveDto);

    PermissionUpdateDto update(Long id, PermissionUpdateDto permissionUpdateDto);

    void deleteById(Long id);
}
