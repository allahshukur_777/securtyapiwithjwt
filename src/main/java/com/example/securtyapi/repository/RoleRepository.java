package com.example.securtyapi.repository;

import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RoleRepository {

    List<RoleDto> findAll(Pageable pageable);

    RoleSaveDto save(RoleSaveDto roleSaveDto);

    RoleUpdateDto update(Long id, RoleUpdateDto roleUpdateDto);

    void deleteById(Long id);
}
