package com.example.securtyapi.repository.impl;

import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import com.example.securtyapi.enums.RoleEnum;
import com.example.securtyapi.exception.UserNotFoundException;
import com.example.securtyapi.mapper.UserMapper;
import com.example.securtyapi.repository.UserRepository;
import com.example.securtyapi.util.ExceptionUtil;
import jooq.tables.pojos.User;
import jooq.tables.records.UserRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.stream.Collectors;
import static jooq.tables.User.USER;

@Repository
@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private final DSLContext dslContext;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public List<UserDto> findAll(Pageable pageable) {
        return dslContext.selectFrom(USER)
                .where(USER.DELETE.equal(false))
                .orderBy(USER.ID)
                .offset(pageable.getPageNumber())
                .limit(pageable.getPageSize())
                .fetchInto(User.class)
                .stream()
                .map(userMapper::toUserDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto findById(Long id) {
        User user = dslContext.selectFrom(USER)
                .where(USER.DELETE.equal(false)
                        .and(USER.ID.equal(id)))
                .fetchOneInto(User.class);
        if (user == null) throw new UserNotFoundException(id);
        return userMapper.toUserDto(user);
    }

    @Override
    public UserDto findUserByUsername(String username) {
        User user = dslContext.select()
                .from(USER)
                .where(USER.DELETE.equal(false)
                        .and(USER.FREEZE_STATUS_ID.equal(1L))
                        .and(USER.USERNAME.equal(username)))
                .fetchOneInto(User.class);
        if (user == null) throw new UsernameNotFoundException(username);
        return userMapper.toUserDto(user);
    }

    @Override
    public UserSaveDto save(UserSaveDto userSave) {
        try {
            UserRecord userRecord = dslContext.insertInto(USER)
                    .set(USER.FULL_NAME, userSave.getFullName())
                    .set(USER.USERNAME, userSave.getUsername())
                    .set(USER.PASSWORD, passwordEncoder.encode(userSave.getPassword()))
                    .set(USER.ROLE, RoleEnum.getEnum(userSave.getRole()).getId())
                    .returning()
                    .fetchOne();
            return userMapper.toUserSaveDto(userRecord);
        } catch (Exception ex) {
            ExceptionUtil.userFkeyException(ex, userSave.getRole());
            throw new RuntimeException();
        }
    }

    @Override
    public UserUpdateDto update(Long id, UserUpdateDto userUpdate) {
        try {
            UserRecord userRecord = dslContext.update(USER)
                    .set(USER.FULL_NAME, userUpdate.getFullName())
                    .set(USER.USERNAME, userUpdate.getUsername())
                    .set(USER.PASSWORD, passwordEncoder.encode(userUpdate.getPassword()))
                    .set(USER.ROLE, RoleEnum.getEnum(userUpdate.getRole()).getId())
                    .where(USER.ID.equal(id))
                    .returning()
                    .fetchOne();
            if (userRecord == null) throw new UserNotFoundException(id);
            return userMapper.toUserUpdateDto(userRecord);
        } catch (Exception ex) {
            ExceptionUtil.userFkeyException(ex, userUpdate.getRole());
            throw new RuntimeException();
        }
    }

    @Override
    public void deleteById(Long id) {
        int userSize = dslContext.update(USER)
                .set(USER.DELETE, true)
                .where(USER.DELETE.equal(false)
                        .and(USER.ID.equal(id)))
                .execute();
        if (userSize == 0) throw new UserNotFoundException(id);
    }
}