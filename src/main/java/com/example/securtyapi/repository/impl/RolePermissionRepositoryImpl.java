package com.example.securtyapi.repository.impl;

import com.example.securtyapi.dto.dto.RolePermissionDto;
import com.example.securtyapi.dto.saveDto.RolePermissionSaveDto;
import com.example.securtyapi.dto.updateDto.RolePermissionUpdateDto;
import com.example.securtyapi.mapper.RolePermissionMapper;
import com.example.securtyapi.repository.RolePermissionRepository;
import jooq.tables.records.RolePermissionRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

import static jooq.tables.Permission.PERMISSION;
import static jooq.tables.Role.ROLE;
import static jooq.tables.RolePermission.ROLE_PERMISSION;

@Repository
@RequiredArgsConstructor
public class RolePermissionRepositoryImpl implements RolePermissionRepository {

    private final DSLContext dslContext;
    private final RolePermissionMapper rolePermissionMapper;

    @Override
    public List<RolePermissionDto> findAll(Pageable pageable) {
        Result<Record> recordResult =  dslContext.select()
                .from(ROLE_PERMISSION)
                .leftJoin(ROLE).on(ROLE_PERMISSION.ROLE_ID.equal(ROLE.ID))
                .leftJoin(PERMISSION).on(ROLE_PERMISSION.PERMISSION_ID.equal(PERMISSION.ID))
                .where(ROLE.DELETE.equal(false)
                        .and(PERMISSION.DELETE.equal(false)))
                .or(ROLE.DELETE.equal(false))
                .orderBy(ROLE.ID, PERMISSION.ID)
                .fetch();
        return rolePermissionMapper.toRolePermission(recordResult);
    }

    @Override
    public RolePermissionDto findById(Long id) {
        Result<Record> recordResult =  dslContext.select()
                .from(ROLE_PERMISSION)
                .leftJoin(ROLE).on(ROLE_PERMISSION.ROLE_ID.equal(ROLE.ID))
                .leftJoin(PERMISSION).on(ROLE_PERMISSION.PERMISSION_ID.equal(PERMISSION.ID))
                .where(ROLE.DELETE.equal(false)
                        .and(PERMISSION.DELETE.equal(false)
                                .and(ROLE.ID.equal(id))))
                .or(ROLE.DELETE.equal(false)
                        .and(ROLE.ID.equal(id)))
                .orderBy(ROLE.ID, PERMISSION.ID)
                .fetch();
        return rolePermissionMapper.toRolePermission(recordResult).get(0);
    }

    @Override
    public RolePermissionDto findRoleAndPermissionByRoleName(String roleName) {
        Result<Record> recordResult =  dslContext.select()
                .from(ROLE_PERMISSION)
                .leftJoin(ROLE).on(ROLE_PERMISSION.ROLE_ID.equal(ROLE.ID))
                .leftJoin(PERMISSION).on(ROLE_PERMISSION.PERMISSION_ID.equal(PERMISSION.ID))
                .where(ROLE.DELETE.equal(false)
                        .and(PERMISSION.DELETE.equal(false)
                                .and(ROLE.NAME.equal(roleName))))
                .or(ROLE.DELETE.equal(false)
                        .and(ROLE.NAME.equal(roleName)))
                .orderBy(ROLE.ID, PERMISSION.ID)
                .fetch();
        return rolePermissionMapper.toRolePermission(recordResult).get(0);
    }

    @Override
    public RolePermissionSaveDto save(RolePermissionSaveDto rolePermissionSave) {
        RolePermissionRecord rolePermissionRecord = dslContext.insertInto(ROLE_PERMISSION)
                .set(ROLE_PERMISSION.ROLE_ID, rolePermissionSave.getRoleId())
                .set(ROLE_PERMISSION.PERMISSION_ID, rolePermissionSave.getPermissionId())
                .returning()
                .fetchOne();
        return rolePermissionMapper.toRolePermissionSave(rolePermissionRecord);
    }

    @Override
    public RolePermissionUpdateDto update(Long id, RolePermissionUpdateDto rolePermissionUpdate) {
        RolePermissionRecord rolePermissionRecord = dslContext.insertInto(ROLE_PERMISSION)
                .set(ROLE_PERMISSION.ID, rolePermissionUpdate.getId())
                .set(ROLE_PERMISSION.ROLE_ID, rolePermissionUpdate.getRoleId())
                .set(ROLE_PERMISSION.PERMISSION_ID, rolePermissionUpdate.getPermissionId())
                .returning()
                .fetchOne();
        return rolePermissionMapper.toRolePermissionUpdate(rolePermissionRecord);
    }

    @Override
    public void deleteById(Long id) {
    }
}
