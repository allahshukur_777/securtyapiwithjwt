package com.example.securtyapi.repository.impl;

import com.example.securtyapi.dto.dto.PermissionDto;
import com.example.securtyapi.dto.saveDto.PermissionSaveDto;
import com.example.securtyapi.dto.updateDto.PermissionUpdateDto;
import com.example.securtyapi.exception.PermissionNotFoundException;
import com.example.securtyapi.mapper.PermissionMapper;
import com.example.securtyapi.repository.PermissionRepository;
import jooq.tables.pojos.Permission;
import jooq.tables.records.PermissionRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

import static jooq.tables.Permission.PERMISSION;

@Repository
@RequiredArgsConstructor
public class PermissionRepositoryImpl implements PermissionRepository {

    private final DSLContext dslContext;
    private final PermissionMapper permissionMapper;

    @Override
    public List<PermissionDto> findAll(Pageable pageable) {
        return dslContext.selectFrom(PERMISSION)
                .where(PERMISSION.DELETE.equal(false))
                .orderBy(PERMISSION.ID)
                .fetchInto(Permission.class)
                .stream()
                .map(permissionMapper::toPermissionDto)
                .collect(Collectors.toList());
    }

    @Override
    public PermissionDto findPermissionByPermissionName(String permissionName) {
        Permission permission = dslContext.select()
                .from(PERMISSION)
                .where(PERMISSION.DELETE.equal(false))
                .fetchOneInto(Permission.class);
        return permissionMapper.toPermissionDto(permission);
    }

    @Override
    public PermissionSaveDto save(PermissionSaveDto permissionSave) {
        PermissionRecord permissionRecord = dslContext.insertInto(PERMISSION)
                .set(PERMISSION.NAME, permissionSave.getName())
                .returning()
                .fetchOne();
        return permissionMapper.toPermissionSaveDto(permissionRecord);
    }

    @Override
    public PermissionUpdateDto update(Long id, PermissionUpdateDto permissionUpdate) {
        PermissionRecord permissionRecord = dslContext.update(PERMISSION)
                .set(PERMISSION.NAME, permissionUpdate.getName())
                .where(PERMISSION.ID.equal(id))
                .returning()
                .fetchOne();
        if (permissionRecord == null) throw new PermissionNotFoundException(Math.toIntExact(id));
        return permissionMapper.toPermissionUpdateDto(permissionRecord);
    }

    @Override
    public void deleteById(Long id) {
        int permissionSize = dslContext.update(PERMISSION)
                .set(PERMISSION.DELETE, true)
                .where(PERMISSION.DELETE.equal(false)
                        .and(PERMISSION.ID.equal(id)))
                .execute();
        if (permissionSize == 0) throw new PermissionNotFoundException(Math.toIntExact(id));
    }
}
