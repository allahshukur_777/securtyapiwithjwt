package com.example.securtyapi.repository;

import com.example.securtyapi.dto.dto.CashierDto;
import com.example.securtyapi.dto.saveDto.CashierSaveDto;
import com.example.securtyapi.dto.updateDto.CashierUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CashierRepository {

    List<CashierDto> findAll(Pageable pageable);

    CashierDto findById(Long id);

    CashierDto findCashierByUsername(String username);

    CashierSaveDto save(CashierSaveDto cashierSaveDto);

    CashierUpdateDto update(Long id, CashierUpdateDto cashierUpdateDto);

    void deleteById(Long id);
}
