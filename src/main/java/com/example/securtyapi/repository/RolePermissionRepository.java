package com.example.securtyapi.repository;

import com.example.securtyapi.dto.dto.RolePermissionDto;
import com.example.securtyapi.dto.saveDto.RolePermissionSaveDto;
import com.example.securtyapi.dto.updateDto.RolePermissionUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RolePermissionRepository {

    List<RolePermissionDto> findAll(Pageable pageable);

    RolePermissionDto findById(Long id);

    RolePermissionDto findRoleAndPermissionByRoleName(String roleName);

    RolePermissionSaveDto save(RolePermissionSaveDto rolePermissionSaveDto);

    RolePermissionUpdateDto update(Long id, RolePermissionUpdateDto rolePermissionUpdateDto);

    void deleteById(Long id);
}
