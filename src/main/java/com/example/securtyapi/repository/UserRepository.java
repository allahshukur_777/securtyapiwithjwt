package com.example.securtyapi.repository;

import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.dto.saveDto.UserSaveDto;
import com.example.securtyapi.dto.updateDto.UserUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserRepository {

    List<UserDto> findAll(Pageable pageable);

    UserDto findById(Long id);

    UserDto findUserByUsername(String username);

    UserSaveDto save(UserSaveDto userSaveDto);

    UserUpdateDto update(Long id, UserUpdateDto userUpdateDto);

    void deleteById(Long id);
}
