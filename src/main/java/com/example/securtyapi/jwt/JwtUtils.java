package com.example.securtyapi.jwt;

import com.example.securtyapi.auth.AuthUserDetails;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.time.LocalDate;
import java.util.Date;

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtUtils {

    private final JwtConfig jwtConfig;
    private final SecretKey secretKey;

    public String generateToken(Authentication authentication) {
        AuthUserDetails authUserDetails = (AuthUserDetails) authentication.getPrincipal();
        java.sql.Date date = java.sql.Date.valueOf(LocalDate.now().plusDays(jwtConfig.getTokenExpirationDays()));
        return jwtConfig.getTokenPrefix() + " " + Jwts.builder()
                .setSubject(authUserDetails.getUsername())
                .claim("authorities", authUserDetails.getAuthorities())
                .setIssuedAt(new Date())
                .setExpiration(date)
                .signWith(secretKey, SignatureAlgorithm.HS256)
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parserBuilder().setSigningKey(secretKey).build().parseClaimsJws(token);
            return true;
        } catch (Exception e) {
            log.error("invalid token"); // Xususi Exception yaradacam
        }
        return false;
    }

    public String getUsernameFromJwtToken(String jwtToken) {
        return Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .build()
                .parseClaimsJws(jwtToken)
                .getBody()
                .getSubject();
    }
}