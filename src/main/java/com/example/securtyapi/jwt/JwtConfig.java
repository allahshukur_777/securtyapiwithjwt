package com.example.securtyapi.jwt;

import com.google.common.net.HttpHeaders;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class JwtConfig {

    @Value("${jwt.secretKey}")
    private String secretKey;
    @Value("${jwt.token-prefix}")
    private String tokenPrefix;
    @Value("${jwt.expiration.date}")
    private Integer tokenExpirationDays;

    public String getAuthorizationHeader() {
        return HttpHeaders.AUTHORIZATION;
    }
}
