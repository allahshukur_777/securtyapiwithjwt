package com.example.securtyapi.auth;

import com.example.securtyapi.dto.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class AuthUserDetails implements UserDetails {

    private String username;
    private String password;
    private Collection<? extends GrantedAuthority> grantedAuthorities;

    public static AuthUserDetails fromUserDtoToAuthUserDetails(UserDto userDto, List<String> permissions) {
        AuthUserDetails authUserDetails = new AuthUserDetails();
        authUserDetails.username = userDto.getUsername();
        authUserDetails.password = userDto.getPassword();
        authUserDetails.grantedAuthorities = getAuthories(userDto, permissions);
        return authUserDetails;
    }

    public static List<SimpleGrantedAuthority> getAuthories(UserDto userDto, List<String> permissions) {
        List<SimpleGrantedAuthority> simpleGrantPermissions = permissions.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
        simpleGrantPermissions.add(new SimpleGrantedAuthority("ROLE_" + userDto.getRole()));
        return simpleGrantPermissions;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
