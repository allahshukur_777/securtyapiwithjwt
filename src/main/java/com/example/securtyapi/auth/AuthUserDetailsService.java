package com.example.securtyapi.auth;

import com.example.securtyapi.dto.dto.RolePermissionDto;
import com.example.securtyapi.dto.dto.UserDto;
import com.example.securtyapi.repository.RolePermissionRepository;
import com.example.securtyapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AuthUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;
    private final RolePermissionRepository rolePermissionRepository;

    @Override
    public AuthUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDto userDto = userRepository.findUserByUsername(username);
        RolePermissionDto permissions = rolePermissionRepository.findRoleAndPermissionByRoleName(userDto.getRole());
        return AuthUserDetails.fromUserDtoToAuthUserDetails(userDto, permissions.getPermissionName());
    }
}
