package com.example.securtyapi.enums;

import com.example.securtyapi.exception.RolesNotFoundException;

public enum RoleEnum {
    ADMIN(1),
    USER(2),
    CASHIER(3);

    private final Integer id;

    RoleEnum(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public static RoleEnum getEnum(Integer value) {
        switch (value) {
            case 1:
                return ADMIN;
            case 2:
                return USER;
            case 3:
                return CASHIER;
            default:
                throw new RolesNotFoundException(value);
        }
    }
}
