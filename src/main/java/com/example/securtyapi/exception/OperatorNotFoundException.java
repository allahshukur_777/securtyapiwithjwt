package com.example.securtyapi.exception;

public class OperatorNotFoundException extends RuntimeException {

    public OperatorNotFoundException(Long id) {
        super(id + " Operator not found");
    }
}
