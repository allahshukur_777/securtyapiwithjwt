package com.example.securtyapi.exception;

public class AgentNotFoundException extends RuntimeException {

    public AgentNotFoundException(Long id) {
        super(id + " Agent not found");
    }
}
