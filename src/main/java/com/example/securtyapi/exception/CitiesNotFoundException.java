package com.example.securtyapi.exception;

public class CitiesNotFoundException extends RuntimeException {

    public CitiesNotFoundException(Long id) {
        super(id + " Cities not found");
    }
}
