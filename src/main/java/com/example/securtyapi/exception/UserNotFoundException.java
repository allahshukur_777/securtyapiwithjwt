package com.example.securtyapi.exception;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(Long id) {
        super(id + " User not found");
    }
}
