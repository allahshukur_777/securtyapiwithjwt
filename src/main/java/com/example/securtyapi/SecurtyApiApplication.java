package com.example.securtyapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurtyApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurtyApiApplication.class, args);
    }

}
