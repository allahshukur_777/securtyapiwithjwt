package com.example.securtyapi.swagger;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class SwaggerPageable {

    @ApiParam(example = "0")
    private Integer page;

    @ApiParam(example = "0")
    private Integer size;
}
