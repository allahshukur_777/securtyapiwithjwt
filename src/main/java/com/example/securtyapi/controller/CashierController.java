package com.example.securtyapi.controller;

import com.example.securtyapi.dto.dto.CashierDto;
import com.example.securtyapi.dto.saveDto.CashierSaveDto;
import com.example.securtyapi.dto.updateDto.CashierUpdateDto;
import com.example.securtyapi.service.CashierService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cashier")
public class CashierController {

    private final CashierService cashierService;

    @GetMapping
    @PreAuthorize("hasAuthority('CASHIER_READ')")
    public List<CashierDto> getAll(@PageableDefault(size = 5) Pageable pageable) {
        return cashierService.getAll(pageable);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('CASHIER_READ')")
    public CashierDto get(@PathVariable Long id) {
        return cashierService.get(id);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('CASHIER_WRITE')")
    public CashierSaveDto save(@Valid @RequestBody CashierSaveDto cashierSave) {
        return cashierService.save(cashierSave);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('CASHIER_PUT')")
    public CashierUpdateDto update(@PathVariable Long id, @Valid @RequestBody CashierUpdateDto cashierUpdate) {
        return cashierService.update(id, cashierUpdate);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('CASHIER_DELETE')")
    public void deleteById(@PathVariable Long id) {
        cashierService.deleteById(id);
    }
}
