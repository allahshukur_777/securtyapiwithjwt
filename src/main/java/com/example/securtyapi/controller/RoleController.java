package com.example.securtyapi.controller;

import com.example.securtyapi.dto.dto.RoleDto;
import com.example.securtyapi.dto.saveDto.RoleSaveDto;
import com.example.securtyapi.dto.updateDto.RoleUpdateDto;
import com.example.securtyapi.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/role")
public class RoleController {

    private final RoleService roleService;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    public List<RoleDto> getAll(@PageableDefault(size = 5) Pageable pageable) {
        return roleService.getAll(pageable);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public RoleSaveDto save(@Valid @RequestBody RoleSaveDto roleSave) {
        return roleService.save(roleSave);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public RoleUpdateDto update(@PathVariable Long id, @Valid @RequestBody RoleUpdateDto roleUpdate) {
        return roleService.update(id, roleUpdate);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public void deleteById(@PathVariable Long id) {
        roleService.deleteById(id);
    }
}
