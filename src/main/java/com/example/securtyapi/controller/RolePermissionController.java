package com.example.securtyapi.controller;

import com.example.securtyapi.dto.dto.RolePermissionDto;
import com.example.securtyapi.dto.saveDto.RolePermissionSaveDto;
import com.example.securtyapi.dto.updateDto.RolePermissionUpdateDto;
import com.example.securtyapi.service.RolePermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/role/permission")
public class RolePermissionController {

    private final RolePermissionService rolePermissionService;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    public List<RolePermissionDto> getAll(@PageableDefault(size = 5) Pageable pageable) {
        return rolePermissionService.getAll(pageable);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    public RolePermissionDto get(@PathVariable Long id) {
        return rolePermissionService.get(id);
    }

    @GetMapping("/roleName/{roleName}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_USER')")
    public RolePermissionDto get(@PathVariable String roleName) {
        return rolePermissionService.getByRoleName(roleName);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public RolePermissionSaveDto save(@Valid @RequestBody RolePermissionSaveDto rolePermissionSave) {
        return rolePermissionService.save(rolePermissionSave);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public RolePermissionUpdateDto update(@PathVariable Long id,
                                          @Valid @RequestBody RolePermissionUpdateDto rolePermissionUpdate) {
        return rolePermissionService.update(id, rolePermissionUpdate);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public void deleteById(@PathVariable Long id) {
        rolePermissionService.deleteById(id);
    }
}
